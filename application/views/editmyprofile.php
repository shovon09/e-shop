<html>
	<head>
		<script type="text/javascript">
			function showrepass() {

				document.getElementById("repass").style.display = "block";

			}
		</script>
	</head>
	<body>
		<br />
		<div class="title">
			<span class="title_icon"><img src="" alt="" title="" /> My Profile </span>
		</div>
		<table id="ver-minimalist" summary="Delivery Areas">
			<thead>
				<tr>
					<th scope="col">Information</th>
					<th scope="col">Result</th>
				</tr>
			</thead>
			<?php echo form_open('http://localhost/online/index.php/customer/updatemyprofile');?>
			<tbody>
				<tr>
					<td>Date Of Registration </td><td><?php echo $customer["date"]
					?></td>
				</tr>
				<tr>
					<td>Customer ID</td><td><?php echo $customer["customer_id"]
					?></td>
				</tr>
				<tr>
					<td> Name </td><td>
					<input name="name" value="<?php echo $customer["name"]?>" />
					</td>
				</tr>
				<tr>
					<td>Country </td><td>
					<input name="country" value="<?php echo $customer["country"]?>" />
					</td>
				</tr>
				<tr>
					<td>State/Division </td><td>
					<input name="state"  value="<?php echo $customer["state"]?>"/>
					</td>
				</tr>
				<tr>
					<td>Full Address</td><td>
					<input name="address" value="<?php echo $customer["address"]?>" />
					</td>
				</tr>
				<tr>
					<td>Contact Number </td><td>
					<input name="contact_no"  value="<?php echo $customer["contact_no"]?>"/>
					</td>
				</tr>
				<tr>
					<td>E-mail Id </td><td>
					<input name="email" value="<?php echo $customer["email"]?>" />
					</td>
				</tr>
				<tr>
					<td>Password </td><td>
					<input type="password" onkeyup="showrepass()"  name="password" value="<?php echo $customer["password"]?>" />
					</td>
				</tr>
				<tr id="repass" style="display: none">
					<td>Confirm Password </td><td>
					<input type="password"  name="confirmpass" value="" />
					</td>
				</tr>
				<tr>
					<td>Account Number </td><td>
					<input name="account_num" value="<?php echo $customer["account_num"]?>" />
					</td>
				</tr>
				<tr>
					<td>Total Cash in Account</td><td><?php echo $customer["total_credit"] . " BDT";?></td>
				</tr>
				<tr>
					<td>Number of Order</td><td><?php echo $customer["num_of_purchase"]
					?></td>
				</tr>
				<tr>
					<td>Number Of Product Purchased</td><td><?php echo $customer["total_product_purchased"]
					?></td>
				</tr>
				<tr>
					<br />
					<br />
					<td>
					<br />
					<br />
					<input type="submit" value="Update Profile" />
					</td>
				</tr>
			</tbody>
			<?php echo form_close();?>
		</table>
	</body>
</html>

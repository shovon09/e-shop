<div id="infowrap">
	<div id="infobox">
		<h3>Last 5 Orders</h3>
		<table>
			<thead>
				<tr>
					<th>Purchase ID </th>
					<th>Customer ID </th>
					<th>Date</th>
					<th>Items</th>
					<th>Grand Total(Tk.)</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$this -> load -> model('edata');

				$data = $this -> edata -> getlast5order();
				foreach ($data as $order) {
					echo '<tr>';
					echo '<td>' . $order -> purchase_id . '</td>';
					echo '<td><a href="#">' . $order -> customer_id . '</a></td>';
					echo '<td>' . $order -> date . '</td>';
					echo '<td>' . $order -> total_quantity . '</td>';
					echo '<td>' . $order -> sales . '</td>';
					echo '</tr>';

				}
				?>
			</tbody>
		</table>
	</div>
	</div>
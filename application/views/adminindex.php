<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>e-Shop Admin</title>
		<link rel="stylesheet" type="text/css" href="http://localhost/online/admin_css/theme.css" />
		<link rel="stylesheet" type="text/css" href="http://localhost/online/admin_css/style.css" />
		<script>
			var StyleFile = "theme" + document.cookie.charAt(6) + ".css";
			document.writeln('<link rel="stylesheet" type="text/css" href="http://localhost/online/admin_css/' + StyleFile + '">');

		</script>
		<!--[if IE]>
		<link rel="stylesheet" type="text/css" href="http://localhost/online/admin_css/ie-sucks.css" />
		<![endif]-->
	</head>
	<body>
		<div id="container">
			<div id="header">
				<h2>eShop Admin Panel</h2>
				<div id="topmenu">
					<ul>
						<li class="">
							<a href="http://localhost/online/index.php/admin/index">Dashboard</a>
						</li>
						<li>
							<a href="http://localhost/online/index.php/admin/orders">Orders</a>
						</li>
						<li>
							<a href="http://localhost/online/index.php/admin/users">Users</a>
						</li>
						<li>
							<a href="http://localhost/online/index.php/admin/manage">Manage</a>
						</li>
						<li>
							<a href="http://localhost/online/index.php/admin/message">Message</a>
						</li>
						<li>
							<a href="http://localhost/online/index.php/admin/logout">Logout</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="top-panel">
				<div id="panel">
					<?php  echo $panel;?>
				</div>
			</div>
			<div id="wrapper">
				<div id="content">
					<?php  //this shownig the overview of the store ...
					echo $msg;
					?>
					<?php
					echo $content;
					?>
				</div>
			</div>
			<div id="sidebar">
				<ul>
					<li>
						<h3><a href="#" class="house">Dashboard</a></h3>
						<ul>
							<li>
								<a href="#" class="report">Sales Report</a>
							</li>
							<li>
								<a href="#" class="report_seo">Business Report</a>
							</li>
							<li>
								<a href="#" class="search">Search</a>
							</li>
						</ul>
					</li>
					<li>
						<h3><a href="#" class="folder_table">Orders</a></h3>
						<ul>
							<li>
								<a href="#" class="addorder">New order</a>
							</li>
							<li>
								<a href="#" class="folder">All Order</a>
							</li>
							<li>
								<a href="#" class="folder">Cancel Request</a>
							</li>
							<li>
								<a href="#" class="search">Search Order</a>
							</li>
						</ul>
					</li>
					<li>
						<h3><a href="#" class="manage">Manage</a></h3>
						<ul>
							<li>
								<a href="#" class="manage_page">Pages</a>
							</li>
							<li>
								<a href="#" class="cart">Products</a>
							</li>
							<li>
								<a href="#" class="folder">Product categories</a>
							</li>
							<li>
								<a href="#" class="promotions">Promotions</a>
							</li>
							<li>
								<a href="#" class="cart">Buy Products</a>
							</li>
							<li>
								<a href="#" class="folder">Inventory</a>
							</li>
							<li>
								<a href="#" class="folder">Prepaid Card</a>
							</li>
							<li>
								<a href="#" class="shipping">Shipment</a>
							</li>
						</ul>
					</li>
					<li>
						<h3><a href="#" class="user">Users</a></h3>
						<ul>
							<li>
								<a href="#" class="useradd">Add user</a>
							</li>
							<li>
								<a href="#" class="group">All User</a>
							</li>
							<li>
								<a href="#" class="search">Find user</a>
							</li>
							<li>
								<a href="#" class="report">Send Newsletter</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="footer">
			<div id="credits">
				all rights reserved <a href="#"></a>
			</div>
			<div id="styleswitcher">
				<ul>
					<li>
						<a href="javascript: document.cookie='theme='; window.location.reload();" title="Default" id="defswitch">d</a>
					</li>
					<li>
						<a href="javascript: document.cookie='theme=1'; window.location.reload();" title="Blue" id="blueswitch">b</a>
					</li>
					<li>
						<a href="javascript: document.cookie='theme=2'; window.location.reload();" title="Green" id="greenswitch">g</a>
					</li>
					<li>
						<a href="javascript: document.cookie='theme=3'; window.location.reload();" title="Brown" id="brownswitch">b</a>
					</li>
					<li>
						<a href="javascript: document.cookie='theme=4'; window.location.reload();" title="Mix" id="mixswitch">m</a>
					</li>
					<li>
						<a href="javascript: document.cookie='theme=5'; window.location.reload();" title="Mix" id="defswitch">m</a>
					</li>
				</ul>
			</div>
			<br />
		</div>
		</div>
	</body>
</html>
